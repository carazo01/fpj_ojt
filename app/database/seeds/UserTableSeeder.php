<?php

// Composer: "fzaninotto/faker": "v1.3.0"
//use Faker\Factory as Faker;

class UserTableSeeder extends Seeder {

	public function run()
	{

        User::create([
            'username' => 'Carrasco',
            'password' => Hash::make('admin'),
            'access_level' => '1'
        ]);

        User::create([
            'username' => 'Monterola',
            'password' => Hash::make('admin2'),
            'access_level' => '2'
        ]);


	}

}
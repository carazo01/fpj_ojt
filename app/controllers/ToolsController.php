<?php
use FPJ\Repositories\ToolRepository;

class ToolsController extends \BaseController {

	function __construct(ToolRepository $tool){
		$this->tool = $tool;
	}

	/**
	 * Display a listing of the resource.
	 * GET /tools
	 *
	 * @return Response
	 */
	public function index()
	{
		$tools = Tool::all();
		return View::make('inventory.tools')->withTools($tools)->withTitle("Tools Management");
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /tools/create
	 *
	 * @return Response
	 */
	public function create()
	{
		if( Request::ajax() ){
			// return Input::all();
			$res = $this->tool->create(Input::all());
			if($res){
				return json_encode(array( "status_code" => "200", "msg" => "Tools Successfully added." ));
			}
			return json_encode(array( "status_code" => "500", "msg" => "Sorry, we can't process your request right now. Please try again later." ));
		}
	}


	/**
	 * Display the specified resource.
	 * GET /tools/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show()
	{
		$res = $this->tool->show(Input::all());
		return $res;
	}


	/**
	 * Update the specified resource in storage.
	 * PUT /tools/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /tools/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy()
	{
		if( Request::ajax() ){
			$res = $this->tool->destroy(Input::get('id'));

			if($res){
				return json_encode(array("status_code" => "200", "msg" => "Successfully deleted."));
			}
			return json_encode(array( "status_code" => "500", "msg" => "Sorry, we can't process your request right now. Please try again later." ));
		}
	}


	public function get_employees(){
		$res = $this->tool->get_employees();
		return $res->toJson();
	}	

	public function update_inventory(){
		switch (Input::get('option')) {
			case 'borrow':
				$res = $this->tool->borrow(Input::all());
				if($res){
					return json_encode( array( "status_code" => "200" ) );
				}
				break;
			
			default:
				$res = $this->tool->updatequantity(Input::all());
				break;
		}

		if($res){
			return json_encode( array( "status_code" => "200" ) );
		}

		return json_encode( array( "status_code" => "500", "msg" => "Sorry, we can't process your request right now. Please try again later." ) );
	}
}
<?php

class Employee extends \Eloquent {
	protected $guarded = ['id'];

	public function dtr()
	{
		return $this->hasMany('Dtr');
	}

	public function dtrToday()
	{
		return $this->dtr()->where('date_login',date('Y-m-d'));
	}

	public function setLnameAttribute($value)
	{
		$this->attributes['lname'] = ucfirst($value);
	}
	public function setFnameAttribute($value)
	{
		$this->attributes['fname'] = ucfirst($value);
	}

	public function setDateHiredAttribute($value)
	{
		$this->attributes['date_hired'] = date('Y-m-d', strtotime($value));
	}
}
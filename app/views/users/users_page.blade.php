
@extends('layouts.default')

@section('content')

<ul class="breadcrumb breadcrumb-top">




        <li><a href=""></a></li>


</ul>


<div class="block">
        <!-- Title -->
        <div class="block-title">
            <h2><strong></strong></h2>
            <a href="javascript:void(0);" data-target="#manage_users_modal" data-toggle="modal" id="add_user">Add new</a>
        </div>
   <div class="table-responsive">
	<table class="datatable table table-bordered background_white" id="tbl_users">

	    <thead>
	        <td>Username</td>
	        <td>Lastname</td>
	        <td>Firstname</td>
	        <td>Address</td>
	        <td>Action</td>
	    </thead>

        @foreach($users as $user)
            <tr data-id="{{ $user->id }}">
            <td>{{ $user->username}}</td>
            <td>{{ $user->lastname }}</td>
            <td>{{ $user->firstname}}</td>
            <td>{{ $user->address}}</td>
            <td>
              <a href="javascript:void(0);" data-toggle="modal" data-target="#edit_users_modal" data-id="{{ $user->id }}" class="edit_user">Edit</a>
              <a href="javascript:void(0);" data-id="{{ $user->id }}" data-onclick="delete-user">Delete</a>
            </td>
            </tr>
        @endforeach
    </table>
   </div>
  </div>


  @stop
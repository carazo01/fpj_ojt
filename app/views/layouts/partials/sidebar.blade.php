<div id="sidebar">
            <!-- Wrapper for scrolling functionality -->
            <div class="sidebar-scroll">
                <!-- Sidebar Content -->
                <div class="sidebar-content">
                    <!-- Brand -->
                    <a href="{{route('dashboard')}}" class="sidebar-brand">

                        <img id="logoimg" src="img/fpj-logoSmall.png">

                    </a>
                    <!-- END Brand -->                 

                                        <!-- Sidebar Navigation -->
                    <ul class="sidebar-nav">

                            <li>
                              <a href="#" class="sidebar-nav-menu">
                                <i class="fa fa-angle-left sidebar-nav-indicator"></i>
                                <i class="gi gi-user sidebar-nav-icon"></i>Inventory
                              </a>
                              <ul>
                                <li>
                                  <a href="{{ URL::to('categories') }}">Categories</a>
                                </li>
                                <li>
                                  <a href="{{ route('tools_path') }}">Tools</a>
                                </li>
                                <li>
                                  <a href="{{ URL::to('inventory') }}">Inventory</a>
                                </li>
                                <li>
                                  <a href="{{ URL::to('reports') }}">Reports</a>
                                </li>
                              </ul>
                            </li>

                            <li>
                            <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator"></i><i class="gi gi-user sidebar-nav-icon"></i>Payroll</a>
                                                        <ul>
                                                                <li>
                                    <a href="{{route('employees')}}">Employee</a>
                                                                    </li>
                                                                <li>

                                    <a href="{{route('payroll')}}">Payroll</a>
                                                                    </li>

                                                            </ul>
                                                    </li>

                                                                    <li>
                            <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator"></i><i class="gi gi-group sidebar-nav-icon"></i>Manage Users</a>
                                                        <ul>

                                                                     <li>
                                    <a href="{{route('users.users_page')}}"  >Users</a>

                                                                </li>


                                                            </ul>
                                                    </li>
                                                                    </ul>
                    <!-- END Sidebar Navigation -->
                    
                </div>
                <!-- END Sidebar Content -->
            </div>
            <!-- END Wrapper for scrolling functionality -->

             

</div>


$(document).ready(function(){
    $('.datatable').dataTable();
    $('.datepicker').datepicker();


    /** Initialize your datatables here */
    var tools_table = $('#inner_tools_table').DataTable( {
        ajax : "ajaxGetTools?option=all"
    });

    var borrowtools_table = $("#borrow_tools_table").DataTable({
        ajax : "ajaxGetTools?option=borrowing"
    });

    /** end */


    $("a[data-onclick='delete-category']").click(function(){
        console.log( $(this).attr("data-id") );

        $this = $(this);

        var c = confirm("Are you sure you want to delete this category?");
        if( c == true ){
            $.ajax({
                url : "ajaxDeleteCategory",
                type : "get",
                data : { "id" : $(this).attr("data-id") }
            }).done(function (data){
                $this.parent('td').parent('tr').fadeIn(function(){
                    $(this).remove();
                    $('#tbl_categories').dataTable();
                });
            });

        }
        
    });


    $("a[data-onclick='delete-employee']").click(function(){
        console.log( $(this).attr("data-id") );

        $this = $(this);

        var c = confirm("Are you sure you want to delete this Employee?");
        if( c == true ){
            $.ajax({
                url : "ajaxDeleteEmployee",
                type : "get",
                data : { "id" : $(this).attr("data-id") }
            }).done(function(data){
                $this.parent('td').parent('tr').fadeIn(function(){
                    $(this).remove();
                    $('#tbl_categories').dataTable();
                });
            });

        }

    });



     $("a[data-onclick='delete-item']").click(function(){
        console.log( $(this).attr("data-id") );

        $this = $(this);

        var c = confirm("Are you sure you want to delete this item?");
        if( c == true ){
            $.ajax({
                url : "ajaxDeleteItem",
                type : "get",
                data : { "id" : $(this).attr("data-id") }
            }).done(function (data){
                $this.parent('td').parent('tr').fadeIn(function(){
                    $(this).remove();
                    $('.datatable').dataTable();
                });
            });

        } 
    });

    $(document).on("click", "a.edit_item", function(){
        var id = $(this).attr("data-id");
        $("#btn_updateItems").attr("data-id", id);
        $.ajax({
            url : "ajaxGetItem",
            type : "post",
            dataType : "json",
            data : { id : id}
        }).done(function (data){
            console.log(data);
            $("#editItem_image").val(data.img);
            $("#editItem_name").val(data.name);
            $("#editItem_description").val(data.description);
            $("#ediIitem_serialNumber").val(data.serial_number);
            $("#editItem_status").val(data.status);
            var category = data.category_id;
            $("#editItem_category option").each(function (){
                if( $(this).val() == category ){
                    $(this).attr("selected", "selected");
                }
            });

        });
    });

    $(document).on("click", "#btn_updateItems", function(){
        var item_id = $(this).attr("data-id");
        var img = $("#editItem_image").val();
        var name = $("#editItem_name").val();
        var description = $("#editItem_description").val();
        var serial_number = $("#ediIitem_serialNumber").val();
        var status = $("#editItem_status").val();
        var category_id = $("#editItem_category option:selected").val();

        var data = {
            item_id : item_id,
            img : img,
            name : name,
            description : description,
            serial_number : serial_number,
            status : status,
            category_id : category_id
        };

        $.ajax({
            url : "ajaxUpdateItem",
            type : "post",
            dataType : "json",
            data : data
        }).done(function (data){
            alert(data.msg);
            if( data.code == "200" ){
                $("#editItem").modal('hide');
                var tr = $("tr[data-id='"+item_id+"']");
                tr.children("td:first").html(img);
                tr.children("td:nth-child(2)").html(name);
                tr.children("td:nth-child(3)").html(description);
                tr.children("td:nth-child(4)").html(serial_number);
                tr.children("td:nth-child(5)").html( (status == '0')? 'Inactive' : 'Active');
            }else{

            }
        });

    });

    $(document).on("click", ".edit_category", function(){
        $.ajax({
            url  : "ajaxGetCategory",
            type : 'get',
            data : { id : $(this).attr("data-id") }
        }).done(function (data){
            $("#editCategory .modal-body").html(data)
        });
    });

    $("#update_categories").click(function(){

        var form = $("#frm_update_category");
        form.submit();

        var id = form.attr("data-id");

        var type = form.children('div').children("select[id='type']").val();
        var description = form.children('div').children("input[id='description']").val();
        var status = form.children('div').children("select[id='status']").val();

        var data = { id : id, type : type, description : description, status : status };

        $.ajax({
            url : 'ajaxUpdateCategory',
            type : 'post',
            dataType : 'json',
            data : data
        }).done(function (data){
            alert(data.msg);
            if(data.code=="200"){
                var tr = $("#tbl_categories tr[data-id='"+id+"']");
                tr.children("td:first").html(type);
                tr.children("td:nth-child(2)").html(description);
                tr.children("td:nth-child(3)").html( (status == '0')? 'No' : 'Yes' );
                $("#editCategory").modal('hide');
            }else{
                $("#editCategory").modal('hide');
            }
        });
    });


    $(document).on("click", "#save_category", function(){
        var type = $("#category_type").val();
        var description = $("#category_description").val();
        $.ajax({
            url : "categories/add",
            type : "post",
            dataType : "json",
            data : {description : description, type : type}
        }).done(function(response){
            console.log(response)
            alert(response.msg);
            if(response.code === "200"){
                location.reload();
                $("#add_category_modal").modal('hide');
            }
        });
    });

    $(document).on("click", "#add_user", function() {
        $('#inputPassword').parent().show();
        $('#inputConfirmPassword').parent().show();
        $("#inputLastname").val("");
        $("#inputFirstname").val("");
        $("#inputMiddlename").val("");
        $("#inputAddress").val("");
        $("#inputBirthday").val("");
        $("#inputUsername").val("");
    });

    $(document).on("click", ".edit_user", function(){
        var id = $(this).data("id");
        //$(this).closest('form').attr("data-operation", "edit");
        //$(this).closest('form').attr("data-id", id);
        var form =  $("#editUser");
        console.log(form);
        $.ajax({
            url : 'users/show',
            type : 'post',
            dataType : 'json',
            data : { id : id }
        }).done(function (data){
            console.log(data);
            $("#editId").val(id);
            $("#editLastname").val(data.lastname);
            $("#editFirstName").val(data.firstname);
            $("#editAddress").val(data.address);
            $("#editBirthday").val(data.birthday);
            $("#editUsername").val(data.username);
        });

    });


    $("a[data-onclick='delete-user']").click(function(){
        console.log( $(this).attr("data-id") );

        $this = $(this);

        var c = confirm("Are you sure you want to delete this user?");
        if( c == true ){
            $.ajax({
                url : "users/delete",
                type : "post",
                data : { "id" : $(this).attr("data-id") }
            }).done(function(data){
                $this.parent('td').parent('tr').fadeIn(function(){
                    $(this).remove();
                    $('#tbl_users').dataTable();
                });
            });

        }

    });


    // $(document).on("click", ".update-user", function(){
        
    //     var lastname = $("#inputLastname").val();
    //     var firstname = $("#inputFirstname").val();
    //     var middlename = $("#inputMiddlename").val();
    //     var address = $("#inputAddress").val();
    //     var birthday = $("#inputBirthday").val();
    //     var username = $("#inputUsername").val();
    //     var password = $("#inputPassword").val();
    //     var data = {
    //         id:id,
    //         lastname : lastname,
    //         firstname : firstname,
    //         middlename : middlename,
    //         address : address,
    //         birthday : birthday,
    //         username : username,
    //         password : password

    //     }
    //     console.log(data);
    //     $.ajax({
    //         url : "users/edit",
    //         type : "post",
    //         dataType : "json",
    //         data : data
    //     }).done(function(response){
    //         console.log(response)
    //         alert(response.msg);
    //         if(response.code === "200"){
    //             location.reload();
    //             $("#manage_users_modal").modal('hide');
    //         }
    //     });
    // });?


    //start of saving user
  /*  $(document).on("submit", "#form-addUser", function(e){
        e.preventDefault();
        var lastname = $("#inputLastname").val();
        var firstname = $("#inputFirstname").val();
        var middlename = $("#inputMiddlename").val();
        var address = $("#inputAddress").val();
        var birthday = $("#inputBirthday").val();
        var username = $("#inputUsername").val();
        var password = $("#inputPassword").val();
        

        if( $(this).data("operation") == "add" ){
            var data = {
                lastname : lastname,
                firstname : firstname,
                middlename : middlename,
                address : address,
                birthday : birthday,
                username : username,
                password : password

            }
            console.log(data);
            $.ajax({
                url : "users/add",
                type : "post",
                dataType : "json",
                data : data
            }).done(function(response){
                console.log(response)
                alert(response.msg);
                if(response.code === "200"){
                    location.reload();
                    $("#manage_users_modal").modal('hide');
                }
            });
        }else{
            var id = $(this).data("id");
            var data = {
            id:id,
            lastname : lastname,
            firstname : firstname,
            middlename : middlename,
            address : address,
            birthday : birthday,
            username : username,
            password : password
        }
        console.log(data);
        $.ajax({
            url : "users/edit",
            type : "post",
            dataType : "json",
            data : data
        }).done(function(response){
            console.log(response)
            alert(response.msg);
            if(response.code === "200"){
                location.reload();
            }

            $("#save_addUser").attr("data-operation", "add");
        });
        }
    });*/
    //end saving user


    $(document).on("click", "#save_item", function(){
        var img = $("#item_image").val();
        var name = $("#item_name").val();
        var description = $("#item_description").val();
        var serial_number = $("#item_serialNumber").val();
        var status = $("#item_status").val();
        var category_id = $("#item_category").val();

        var data = { 
            img : img, 
            name : name, 
            description : description, 
            serial_number : serial_number, 
            status : status, 
            category_id : category_id
        };

        console.log(data);
        $.ajax({
            url : "items/add",
            type : "post",
            dataType : "json",
            data : data
        }).done(function(response){
            console.log(response)
            alert("alert1 "+response.msg);
            if(response.code === "200"){
                $("#add_item_modal").modal('hide');
                location.reload();
            }
        });
    });


    var quantity = '0';

    $("#tool_type").change(function (){
        $this = $(this);
       
        if( $this.val() == "0" ){
            quantity = parseFloat( $("#tool_quantity").val() );
            // the selected item is Non-Consumable
            $("#serials").html("");
            $("#serials").fadeIn(function (){
                $("#serials").html("");
                for( var i=0; i<quantity; i++ ){
                    console.log('i='+i+" and q="+quantity);
                    $("#serials").append('<input type="text" placeholder="Serial #'+(i+1)+'" name="serial_number[]" class="form-control">');
                }
            });
            $('div[data-for="consumable"]').fadeOut();
            $('div[data-for="nonconsumable"]').fadeIn();
        }else{
            $("#serials").fadeOut(function(){
                $("#serials").html("");
                quantity = 0;
            });
            $('div[data-for="consumable"]').fadeIn();
            $('div[data-for="nonconsumable"]').fadeOut();
        }
    });

    $("#tool_quantity").change(function (){
        var $this = $(this);
        
        if( $("#tool_type").val() == "0" ){
            quantity = parseFloat( $this.val() );
            $("#serials").html("");
            
            $("#serials").fadeIn(function (){
                for( var i=0; i<quantity; i++ ){
                    $("#serials").append('<input type="text" placeholder="Serial #'+(i+1)+'" name="serial_number[]" class="form-control">');
                }
            });
        }
    });

    var attempt = 0;
    $("#save_tool").click(function(){
        var $this = $(this);
        if( attempt < 1 ){
            
            $this.text("Saving...");
            $.post('ajaxAddtool', $("#form_addtool").serialize(), function (data) {
                alert(data.msg);
                if( data.status_code == "200" ){
                    $("#add_tool_modal").modal('hide');
                }
                $this.text("Save");
                tools_table.ajax.reload();
            }, 'json');
            
            attempt++;
        }else{
            attempt = 0;
        }
    });

    $("#update_tool").click(function(){
        var $this = $(this);
        $this.text("Updating...");
        $.post('ajax', $("#form_edittool").serialize(), function (data) {
            alert(data.msg);
            if( data.status_code == "200" ){
                $("#add_tool_modal").modal('hide');
            }
            $this.text("Save");
            tools_table.ajax.reload();
        }, 'json');
    });

    $(document).on("click", "a.edit_tool", function(){
        var $this = $(this);
        var res = fn.ajax("ajaxGetTools", {option : 'specific', id : $this.data('id')}, "get", "json");
        res.done(function (data){
            var form = $("#form_edittool div.form-group");
            form.children("input[name='id']").val(data.id);
            form.children("input[name='name']").val(data.name);
            form.children("textarea[name='description']").html(data.description);
            
            if( data.is_consumable == '0' ){
                $("div[data-for='consumable']").hide();
                $("div[data-for='nonconsumable']").show();
                form.children("input[name='serial_number']").val(data.serial_number);
                form.children('input[name="tool_quantity"]').parent('div').hide();

            }else{
                $("div[data-for='consumable']").show();
                $("div[data-for='nonconsumable']").hide();
                form.children('input[name="tool_quantity"]').parent('div').show();
                form.children('input[name="tool_quantity"]').val(data.quantity);

                form.children('select[name="unit"] option').removeAttr('selected');
                form.children('select[name="unit"] option[value="'+data.unit+'"]').attr("selected", "selected");
            }
            
        });
    });

    $(document).on("click", "a.tool-actions[data-onclick='delete-tool']", function(){
        var $this = $(this);
        var id = $this.data("id");
        
        var c = confirm("Are you sure to delete this tool?");

        if(c){
            $this.parent("td").parent("tr").addClass("warning");
            $.ajax({
                url : "ajaxDeleteTool",
                type : "post",
                dataType : "json",
                data : {id : id}
            }).done(function (data){
               
                if(data.status_code == "200"){
                    tools_table.ajax.reload();
                    $this.parent("td").parent("tr").fadeOut(function(){
                        $(this).remove();
                    });
                }else{
                    alert(data.msg);
                }
            });
        }

    });



    $(document).on("click", ".compute_payroll", function(){
        var name = $.trim($(this).closest('tr').children().html());
        var position = $.trim($(this).closest('tr').children().eq(2).html());

        $('#employeeName').val(name);
        $('#position').val(position);
    });


    $(document).on("change", "#toDate", function(){
        var toDate = $(this).val();
        var fromDate = $("#fromDate").val();

        var form = $("#frm_computepayroll").serialize();
        var pay = 0;
        var attempt = 0;
        var q = $.Deferred();

        $.ajax({
            url : "payroll",
            type : "post",
            dataType : "json",
            data : form,
            success : function (data){
            q.resolve(data);

            }});

        q.promise().then(function(data){

               $("#employeeName").val(data.fname+" "+data.lname);
                $("#position").val(data.position);
                $("#position").val(data.position);
                $("#daysWorked").val(data.daysOfWork);

                var overtime = parseInt(data.overtime)/60;
                var overtime_pay = (parseFloat(overtime)/8) * parseInt(data.rate);
                $("#overtimeHrs").val( overtime );

                pay = parseInt(data.rate) * parseInt(data.daysOfWork);
                console.log(pay);
                if( $('#rate_pay').is(':visible') == false ){
                  $("label[for='rate']").after('<b class="pull-right" id="rate_pay">'+pay+'</b>');
                }

                if( $('#overtime').is(':visible') == false ){
                    $("label[for='overtimePay']").after('<b class="pull-right" id="overtime">'+overtime_pay+'</b>')
                }

                q.reject();                
        });
    });

    $("#frm_computepayroll").on("submit", function(){
        var cola = parseFloat( $("#cola").val() );
        var sss_premium = parseFloat( $("#sss_premium").val() );
        var sss_loan = parseFloat( $("#sss_loan").val() );
        var phic = parseFloat( $("#phic").val() );
        var pagibig_premium = parseFloat( $("#pagibig_premium").val() );
        var pagibig_loan = parseFloat( $("#pagibig_loan").val() );
        var rate_pay = parseFloat( $("#rate_pay").html() );
        var overtime = parseFloat( $("#overtime").html() );
        var gross_pay = rate_pay + overtime + cola;

        var deductions = sss_premium+sss_loan+phic+pagibig_loan+pagibig_premium;
        var net_pay = gross_pay - deductions;
        $("#net_pay").html(net_pay);
        console.log("gross_pay: "+gross_pay+"\ndeductions: "+deductions+"\nnet_pay: "+net_pay);
    });

    $(document).on("click", "a.borrow_tool", function(){
        
            var div = "#tool_information";
            $(div+" input#tool_name").val( $(this).data("name") );
            $(div+" textarea#tool_description").val( $(this).data("description") );
            $(div+" input#tool_serial").val( $(this).data("serial") );
            $(div+" input[name='tool_id']").val( $(this).data("id") );
            $(div+" span#unit").html( $(this).data("unit") );

        // get fresh list of employees
        var res = fn.ajax("get_employees", {}, "post", "json");
        res.done(function (data){
            $("#responsible_employees").html("");
            $.each(data, function (i, row){
                $("#responsible_employees").append('<option value="'+row.id+'">'+row.fname+' '+row.lname+'</option>')
            });
        });

    });

    $(document).on("click", "a.update_quantity", function(){
        $("#div_updatequantity input[name='id']").val( $(this).data("id") );
    });

    $("#form_borrowtool").submit(function(){
        var form = $(this).serialize();
        var res = fn.ajax("borrow", form, "post", "json");
        $("#btn_borrow_tool").html("Please wait...");
        res.done(function (response){
            console.log(response);
            if( response.status_code == "200" ){
                $("#borrowtool").modal('hide');
                borrowtools_table.ajax.reload();
            }
        });
    });

    $("#form_updatequantity").submit(function(){
        var form = $(this).serialize();
        var res = fn.ajax("updatequantity", form, "post", "json");
        res.done(function (response){
            console.log(response);
            if(response.status_code == "200"){
                borrowtools_table.ajax.reload();
                $("#updatequantity").modal('hide');
            }
        });
    });

    $("a.table-refresher").click(function(){
        var tbl = $(this).data("table");
        if( tbl == "borrow_tools_table" ){
            borrowtools_table.ajax.reload();
        }
    });

    $(document).on("click", "a.borrowed", function(){
        $("#return_tool").attr('data-master', $(this).data('master'));
        var res = fn.ajax("get_employees", {}, "post", "json");
        res.done(function (data){
            $("#getborrowers div.modal-body").html("");
            $.each(data, function (i, row){
                $("#getborrowers div.modal-body").append('<a class="employee-list" href="employee/'+row.id+'" target="_blank">'+row.fname+' '+row.lname+'</a><br/>')
            });
        });
    });

    $(document).on("click", "#return_tool", function(){
        var $this = $(this);
        $this.html("Please wait...");
        var master = $(this).data("master");
        var res = fn.ajax("ajax", {'master' : master, 'q' : 'return_tool'}, "post", "json");
        res.done(function (data){
            console.log(data); 
            if( data.status_code == "200" ){
                $("#getborrowers").modal('hide');
                borrowtools_table.ajax.reload();
            }else{
                alert(data.msg);
            }
            $this.html("Return");
        });
    });

    var fn = {
        ajax : function(url, data, type, dataType){
            return  $.ajax({
                url : url,
                type : type,
                data : data,
                dataType : dataType
            });
        }
    };
    
});

<?php
class ItemController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /item
	 *
	 * @return Response
	 */
	public function index()
	{
        $items = Item::with('category')->get();
        $categories = Category::all();
        $categories2 = array();
        foreach ($categories as $category) {
        	$categories2[$category->id] = $category->description;
        }
        // die($categories);
        return View::make('inventory.items')->withTitle('Items')->withItems($items)->withCategories($categories2);
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /item/create
	 *
	 * @return Response
	 */
	public function create()
	{
        $res = Item::create(Input::all());

        if( $res ){
            return json_encode(array( "msg" => "Successfully created!", "code" => "200" ));
        }
        return json_encode(array( "msg" => "Sorry, I can't process your request right now. Try again later.", "code" => "500" ));
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /item
	 *
	 * @return Response
	 */
	/**public function store()
	{
		//
	}
     */

	/**
	 * Display the specified resource.
	 * GET /item/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show(){

		$id = Input::get("id");
        $item = Item::find($id);
        return json_encode($item);
	}

    public function add(){
        return View::make('add_item');
    }


    public function delete(){
        if( Request::ajax() ){
            $id = Input::get('id');
            $item = Item::find($id);
            $res = $item->delete();

            if( $res ){
                Redirect::back()->withFlashMessage('Item successfully deleted.');
            }else{
                Redirect::back()->withFlashMessage("Sorry, we can't process your request right now!");
            }
        }
    }

	public function edit()
	{
		$item = Item::find(Input::get('item_id'));
		$item->img = Input::get('img');
        $item->name = Input::get('name');
        $item->description = Input::get('description');
        $item->serial_number = Input::get('serial_number');
        $item->status = Input::get('status');
        $item->category_id = Input::get('category_id');
		
		$res = $item->save();

        if( $res ){
            return json_encode(array( "msg" => "Successfully created!", "code" => "200" ));
        }
        return json_encode(array( "msg" => "Sorry, I can't process your request right now. Try again later.", "code" => "500" ));
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /item/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	/**public function update($id)
	{
		//
	}
     */

	/**
	 * Remove the specified resource from storage.
	 * DELETE /item/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */


}
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateItemTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */

	public function up()
	{
		Schema::create('items', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('serial_number')->unique();
			$table->string('name');
			$table->string('description');
			$table->string('status');
			$table->string('img');
			$table->boolean('is_active');
            $table->integer('category_id')->index()->unsigned();;
            $table->foreign('category_id')
                ->references('id')->on('categories')
                ->onDelete('cascade');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('item');
	}

}

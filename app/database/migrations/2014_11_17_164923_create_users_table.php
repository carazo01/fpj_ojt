<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $t){
			$t->increments('id');
			$t->string('username', 200);
			$t->string('password', 200);
            $t->string('lastname', 200);
            $t->string('firstname', 200);
            $t->string('middlename', 200);
            $t->string('address', 200);
            $t->string('birthday', 200);
			$t->string('remember_token', 100);
			$t->string('access_level', 200);
            $t->softDeletes();
            $t->timestamps();


		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}

<?php

function isNotLogged($logDetails)
{
    if($logDetails)
    {
       if(($logDetails->login_time != '00:00:00'))
        return 'hide';
    }

}

function isLogged($logDetails)
{
    if($logDetails)
    {
        if($logDetails->login_time != '00:00:00' && $logDetails->logout_time != '00:00:00' )
            return 'hide';
    }
}


function verify_serial($serial_number){
    $res = Tool::where('serial_number', '=', $serial_number)->first();
    if( isset($res->id) ){
        return '0';
    }
    return '1';
}

function pre($str){
    echo '<pre>';
    print_r($str);
    echo '</pre>';
}
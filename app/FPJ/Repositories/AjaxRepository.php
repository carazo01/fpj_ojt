<?php namespace FPJ\Repositories;
use BorrowMaster;

class AjaxRepository{

	public function return_tool($input){
		$master = BorrowMaster::find($input['master']);
		$master->is_returned = 1;
		$res = $master->update();
		if( $res ){
			return true;
		}
		return false;
	}
}
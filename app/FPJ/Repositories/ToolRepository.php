<?php namespace FPJ\Repositories;
use Tool;
use Employee;
use BorrowMaster;
use Borrow;
use DB;

class ToolRepository{

	public function create($input){
		if( $input['is_consumable'] == '0' ){
			// fetch all the serial numbers given
				$success_counter = 0;
				foreach ($input['serial_number'] as $serial => $value) {
					if(verify_serial($value) == '1'){
						$tool = new Tool;
						$tool->name = $input['name'];
						$tool->description = $input['description'];
						$tool->is_consumable = $input['is_consumable'];
						$tool->serial_number = $value;
						$tool->quantity = 1;
						$tool->unit = "pcs";
						$tool->save();
						
						if( $success_counter == count($input['serial_number'])-1 ){
							return true;
						}
						$success_counter++;
					}else{
						return false;
					}
				}
			return false;
		}else{
			$tool = new Tool;
			$tool->name = $input['name'];
			$tool->description = $input['description'];
			$tool->is_consumable = 1;
			$tool->quantity = $input['quantity'];
			$tool->unit = $input['unit'];
			if( $tool->save() ){
				return true;
			}
			return false;
		}
	}

	public function show($input){
		$records = array();

		if( $input['option'] == "all" || $input['option'] == "borrowing" ){
			// $tools = Tool::all();
			$tools = DB::select( DB::raw("SELECT t.*, bm.id as master, bm.is_returned FROM tools as t left join borrow_master as bm on bm.tool_id = t.id ") );

			foreach ($tools as $tool) {
				if( $input['option'] == "all" ){
					$act = '<a class="tool-actions edit_tool" href="javascript:void(0);" data-toggle="modal" data-target="#edit_tool_modal" data-id="'.$tool->id.'">Edit</a>
						<a class="tool-actions" href="javascript:void(0);" data-id="'.$tool->id.'" data-onclick="delete-tool">Delete</a>';
				}else{
					if( $tool->is_returned == 0 && $tool->master != null ){
						$act = '<a class="borrowed" href="javascript:void(0);" data-toggle="modal" data-target="#getborrowers" data-master="'.$tool->master.'">Borrowed</a>';
					}else{
						if($tool->is_consumable == 0){
							$act = '<a class="borrow_tool" href="javascript:void(0);" data-toggle="modal" data-target="#borrowtool" data-id="'.$tool->id.'"  data-name="'.$tool->name.'"  data-description="'.$tool->description.'"  data-quantity="'.$tool->quantity.'"  data-serial="'.$tool->serial_number.'">Borrow</a>';
						}else{
							$act = '<a class="update_quantity" href="javascript:void(0);" data-toggle="modal" data-target="#updatequantity" data-id="'.$tool->id.'"  data-name="'.$tool->name.'"  data-description="'.$tool->description.'"  data-quantity="'.$tool->quantity.'"  data-serial="'.$tool->serial_number.'" data-unit="'.$tool->unit.'">Update Quantity</a>';
						}
					}
				}

				$record = array(
					$tool->name,
					$tool->description,
					($tool->is_consumable == 1) ? 'Consumable' : 'Non-Consumable',
					($tool->serial_number != null )? $tool->serial_number : 'N/A',
					$tool->quantity,
					$tool->unit,
					$act
				);
				array_push($records, $record);
			}
		}else if( $input['option'] == "specific" ){ 
			$tool = Tool::find($input['id']);
			return $tool->toJson();
		}

		return json_encode( array( "data" => $records ) );

	}

	public function destroy($id){
		$tool = Tool::find($id);
		$res = $tool->delete();
		if($res){
			return true;
		}
		return false;
	}


    public function get_employees(){
       return Employee::all();
    }

	public function borrow($input){
		// get the tool
			$tool = $input['tool_id'];

		// save to borrow_master
			$master = new BorrowMaster;
			$master->tool_id = $tool;

			if( $master->save() ){
				foreach ($input['responsible_employees'] as $key => $value) {
					$borrow = new Borrow;
					$borrow->borrow_master_id = $master->id;
					$borrow->employee_id = $value;
					$borrow->save();
				}
			}else{
				return false;
			}

			return true;

		// get all the responsible employees
	}

	public function updatequantity($input){
		$id = $input["id"];
		$tool = Tool::find($id);
		$tool->quantity = $input['new_quantity'];
		if( $tool->save() ){
			return true;
		}
		return false;
	}

	public function update_tool($input){
		$tool = Tool::find($input["id"]);
		$tool->name = $input["name"];
		$tool->description = $input["description"];
		if($input["is_consumable"] == "0"){
			$tool->serial_number = $input["serial_number"];
		}else{
			$tool->quantity = $input["quantity"];
			$tool->unit = $input["unit"];
		}
		if( $tool->update() ){
			return true;
		}else{
			return json_encode( array("status_code"=>"500", "msg" => "Sorry, we can't process your request right now. Please try again later.") );
		}
	}
}
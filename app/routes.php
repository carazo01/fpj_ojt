<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/


//dtr
Route::get('/payroll', [
    'as' => 'payroll',
    'uses' => 'PayrollController@index'
]);
Route::get('/employees', [
    'as' => 'employees',
    'uses' => 'DtrController@employees'
]);

Route::post('/employees', [
    'as' => 'employee.save',
    'uses' => 'DtrController@store'
]);



Route::post('/dtr/login', [
    'as' => 'dtr.login',
    'uses' => 'DtrController@login'
]);

Route::post('/dtr/logout', [
    'as' => 'dtr.logout',
    'uses' => 'DtrController@logout'
]);

Route::get('ajaxDeleteEmployee', [
    'as' => 'employee.delete',
    'uses' => 'DtrController@delete'
]);

Route::get('/', function(){
    return Redirect::to('landing_page');
});

Route::get('users', [
		'as' => 'users.index',
		'uses' => 'UserController@index'

	]);

Route::get('users/{id}', [
		'as' => 'users.show',
		'uses' => 'UserController@show'

	]);

Route::get('NewUser', [
    'as' => 'users.createUser',
    'uses' => 'UserController@createUser'
]);

Route::get('users', [
    'as' => 'users.users_page',
    'uses' =>'UserController@show_usersPage'
    ]);
Route::get('landing_page', [
    'as' => 'landing_page_path',
    'uses' => 'HomeController@showLandingPage'
]);

Route::get('dashboard', [
    'as' => 'dashboard',
    'uses' => 'UserController@dashboard'
]);

Route::get('categories', [
    'as' => 'categories_page_path',
    'uses' => 'CategoriesController@index'
]);

Route::get('items', [
    'as' => 'categories_page_path',
    'uses' => 'ItemController@index'
]);

Route::get('inventory', [
    'as' => 'inventory_page_path',
    'uses' => 'InventoryController@index'
]);

Route::get('tools', [
    'as' => 'tools_path',
    'uses' => 'ToolsController@index'
]);

Route::get('ajaxGetTools', 'ToolsController@show');
Route::post('ajaxAddtool', "ToolsController@create");
Route::post('ajaxDeleteTool', "ToolsController@destroy");

Route::get('categories/add', 'CategoriesController@add');

Route::get('reports', 'ReportsController@index');

Route::post('landing_page', 'UserController@login');
Route::post('categories/add', 'CategoriesController@create');
Route::post('items/add', 'ItemController@create');

Route::post('users/add', 'UserController@create');
Route::post('users/edit', 'UserController@edit');
Route::post('users/show', 'UserController@showUserInfo');
Route::post('users/delete', 'UserController@delete');

Route::post('get_employees', 'ToolsController@get_employees');



Route::get('logout', function(){
     \Auth::logout();

      return  Redirect::to('landing_page');

});

Route::get('ajaxDeleteCategory', 'CategoriesController@delete');
Route::get('ajaxGetCategory', 'CategoriesController@show');
Route::post('ajaxUpdateCategory', 'CategoriesController@edit');

Route::get('ajaxDeleteItem', 'ItemController@delete');
Route::get('ajaxGetItem', 'ItemController@show');
Route::post('ajaxGetItem', 'ItemController@show');
Route::post('ajaxUpdateItem', 'ItemController@edit');

Route::get('ajaxGetUser', 'UserController@showUserInfo');
Route::post('ajaxFetchEmployee', '');


Route::post('payroll', [
    'as' => 'compute.payroll',
    'uses' => 'PayrollController@compute'
]);


// Routes in processing tools borrowing
Route::post("borrow", "ToolsController@update_inventory");
Route::post("updatequantity", "ToolsController@update_inventory");

// Routes for any ajax requests by Dexter Gwapo Bengil 
Route::post('ajax', 'AjaxController@index');

Route::get('try/{serial}', function($serial){
   pre($serial);
   pre(verify_serial($serial));
});
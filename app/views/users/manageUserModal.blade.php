<div class="modal fade" id="manage_users_modal" tabindex="-1" role="dialog" aria-labelledby="manage_users_modalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="manage_users_modalLabel">Add User</h4>
            </div>
            {{Form::open(['action' => 'UserController@create','id' => 'frm-addUser'])}}

            <div class="modal-body">
                    <div class="form-group">
                        <label for="inputLastname">Lastname</label>
                        <input name="lname" type="text" class="form-control" id="inputLastname" placeholder="Enter Lastname">
                    </div>
                    <div class="form-group">
                        <label for="inputFirstname">Firstname</label>
                        <input name="fname" type="text" class="form-control" id="inputFirstname" placeholder="Enter Firstname">
                    </div>
                    <div class="form-group">
                        <label for="inputMiddlename">Middle Name</label>
                        <input name="mname" class="form-control" id="inputMiddlename" placeholder="Enter Middlename">
                    </div>
                    <div class="form-group">
                        <label for="inputAddress">Address</label>
                        <input name="address" type="text" class="form-control" id="inputAddress" placeholder="Enter Address">
                    </div>
                    <div class='form-group' data-date-format="mm-dd-yyyy">
                        <label for="inputBirthday">Birthday</label>
                        <input name="birthday" type='text' class="form-control datepicker" id="inputBirthday" placeholder="Enter Birthday" readonly>
                    </div>
                    <div class="form-group">
                        <label for="inputUsername">Preferred Username</label>
                        <input name="username" type="text" class="form-control" id="inputUsername" placeholder="Enter Username">
                    </div>
                    <div class="form-group">
                        <label for="inputPassword">Password</label>
                        <input name="password" type="password" class="form-control" id="inputPassword" placeholder="Enter Password">
                    </div>
                    <div class="form-group">
                        <label for="inputConfirmPassword">Confirm Password</label>
                        <input name="confirm_password" type="password" class="form-control" id="inputConfirmPassword" placeholder="Confirm Password">
                    </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
              </form>
        </div>
    </div>
</div>
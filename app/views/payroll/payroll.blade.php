
@extends('layouts.default')

@section('content')

    <div class="content-header">
        <div class="header-section">
            <h1>Payroll</h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li>Payroll</li>



        <li><a href="">Employee</a></li>


    </ul>
    <div class="block">
        <!-- Title -->
        <div class="block-title">
            <h2><strong>Employee</strong></h2>
            <a href="javascript:void(0);" data-toggle="modal" data-target="#addEmployee">Add new</a>
        </div>

        <div class="table-responsive">
            <table class="datatable table table-striped table-bordered table-hover table-condensed" id="tbl_categories">

                <thead>
                <td>Name</td>
                <td>Contact Number</td>
                <td>Position</td>
                <!-- <td>Time Tracker</td> -->
                <td>Action</td>
                </thead>

                @foreach($employee as $e)
                    <tr data-id="{{ $e->id }}">
                        <td>{{ $e->fname .  ' ' . $e->lname }}</td>
                        <td>{{ $e->contact_number }}</td>
                        <td>{{$e->position }}</td>
                        <td>
                            <a href="#" data-toggle="modal" data-target="#computePayroll" data-id="{{ $e->id }}" class="compute_payroll btn btn-primary btn-mini"><i class="fa fa-pencil-square-o"></i> Compute Payroll</a>
                        </td>
                    </tr>
                @endforeach

            </table>
        </div>
    </div>





@stop
@section('footer')

    <!-- Modal -->
    <div class="modal fade" id="computePayroll" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Compute Payroll</h4>
                </div>
                <div class="modal-body">

                    <div class="form-horizontal">
                        <div class="form-group ">
                            <div class="col-xs-6">
                                <iframe src="about:blank" id="remember" name="remember" style="display:none;"></iframe>
                                {{Form::open(['route' => 'compute.payroll', 'id'=>'frm_computepayroll', 'target' => 'remember'])}}
                                {{Form::hidden('emp_id',null,['id' => 'emp_id'])}}
                                {{ Form::label('fromDate', 'From:') }}
                                {{ Form::text('fromDate', null, ['id' => 'fromDate', 'class' => 'input-datepicker form-control input-sm']) }}
                            </div>
                            <div class="col-xs-6">
                                {{ Form::label('toDate', 'To:') }}
                                {{ Form::text('toDate', null, ['id' => 'toDate', 'class' => 'input-datepicker form-control input-sm']) }}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                {{ Form::label('employeeName', 'Employee Name:') }}
                                {{ Form::text('employeeName', null, ['id' => 'employeeName', 'class' => 'form-control input-sm',
                                    'placeholder' => 'Employee Name' ]) }}

                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                {{ Form::label('position', 'Position:') }}
                                {{ Form::text('position', null, ['id' => 'position', 'class' => 'form-control input-sm',
                                    'placeholder' => 'Position' ]) }}

                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                {{ Form::label('daysWorked', '# Days of worked:') }}
                                {{ Form::text('daysWorked', null, ['id' => 'daysWorked', 'class' => 'form-control input-sm',
                                    'placeholder' => '# Days of worked:' ]) }}


                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                {{ Form::label('overtimeHrs', '# of Overtime hrs.:') }}
                                {{ Form::text('overtimeHrs', null, ['id' => 'overtimeHrs', 'class' => 'form-control input-sm',
                                    'placeholder' => '# of Overtime hrs.:' ]) }}

                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                {{ Form::label('rate', 'Basic Pay') }}

                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                {{ Form::label('overtimePay', 'Overtime:') }}

                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                {{ Form::label('cola', 'COLA:') }}
                                {{ Form::text('cola', null, ['id' => 'cola', 'class' => 'form-control input-sm',
                                    'placeholder' => '0.00', 'value' => '0' ]) }}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                {{ Form::label('late', 'Late:') }}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                {{ Form::label('SSSPremium', 'SSS Premium:') }}
                                {{ Form::text('sss_premium', null, ['id' => 'sss_premium', 'class' => 'form-control input-sm',
                                    'placeholder' => '0.00', 'value' => '0' ]) }}
                            </div>

                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                {{ Form::label('SSSLoan', 'SSS Loan:') }}
                                {{ Form::text('sss_loan', null, ['id' => 'sss_loan', 'class' => 'form-control input-sm',
                                    'placeholder' => '0.00', 'value' => '0' ]) }}
                            </div>

                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                {{ Form::label('PHIC', 'PHIC Premium:') }}
                                {{ Form::text('phic', null, ['id' => 'phic', 'class' => 'form-control input-sm',
                                    'placeholder' => '0.00', 'value' => '0' ]) }}
                            </div>

                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                {{ Form::label('pagibigPremium', 'HDMF Premium:') }}
                                {{ Form::text('pagibig_premium', null, ['id' => 'pagibig_premium', 'class' => 'form-control input-sm',
                                    'placeholder' => '0.00', 'value' => '0' ]) }}
                            </div>

                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                {{ Form::label('pagibigLoan', 'HDMF Loan') }}
                                {{ Form::text('pagibig_loan', null, ['id' => 'pagibig_loan', 'class' => 'form-control input-sm',
                                    'placeholder' => '0.00', 'value' => '0' ]) }}
                            </div>
                        </div>
                        <div class="form-group" id="result">
                            <div class="col-xs-12">
                                <div class="pull-left">
                                    <strong>Net Pay: </strong>
                                </div>
                                <div class="pull-right">
                                    <strong id="net_pay"></strong>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="sumbit" class="btn btn-primary" id="compute_payroll">Compute</button>
                    {{Form::close()}}

                </div>
            </div>
        </div>
    </div>

    <!-- Modal for Add new Category -->
    <div class="modal fade" id="addEmployee" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add Employee</h4>
                </div>
                <div class="modal-body">
                    {{Form::open(['route' => 'employee.save','id' => 'form-employee'])}}
                        <!-- Last Name Form Input -->
                        <div class="form-horizontal">
                            <div class="form-group">
                                <div class="col-xs-6">
                                    {{ Form::label('fname', 'First Name:') }}
                                    {{ Form::text('fname', null, ['id' => 'fname', 'class' => 'form-control input-sm',
                                    'placeholder' => 'First Name' ]) }}
                                </div>
                                <div class="col-xs-6">
                                    {{ Form::label('lname', 'Last Name:') }}
                                    {{ Form::text('lname', null, ['id' => 'lname', 'class' => 'form-control input-sm',
                                    'placeholder' => 'Last Name' ]) }}
                                </div>
                            </div>
                            <!-- Address Form Input -->
                            <div class="form-group">
                                <div class="col-md-12">
                                    {{ Form::label('address', 'Address:') }}
                                    {{ Form::text('address', null, ['id' => 'address', 'class' => 'form-control' ]) }}
                                </div>
                            </div>
                            <!-- Address Form Input -->
                            <div class="form-group">
                                <div class="col-md-6">
                                    {{ Form::label('contact_number', 'Phone:') }}
                                    {{ Form::text('contact_number', null, ['id' => 'contact_number', 'class' => 'form-control' ]) }}
                                </div>
                            </div>
                            <hr/>
                            <div class="form-group">
                                <div class="col-xs-4">
                                    {{ Form::label('position', 'Position:') }}
                                    {{ Form::text('position', null, ['id' => 'position', 'class' => 'form-control input-sm',
                                    'placeholder' => 'Position' ]) }}
                                </div>

                                    <div class="col-xs-4">
                                        {{ Form::label('rate', 'Daily Rate:') }}
                                        {{ Form::text('rate', null, ['id' => 'rate', 'class' => 'form-control input-sm',
                                        'placeholder' => 'Daily Rate' ]) }}
                                    </div>

                                <div class="col-xs-4">
                                    {{ Form::label('date_hired', 'Date Hired:') }}
                                    {{ Form::text('date_hired', null, ['id' => 'date_hired', 'class' => 'input-datepicker form-control input-sm']) }}
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-4">
                                    {{ Form::label('sss_number', 'SSS #:') }}
                                    {{ Form::text('sss_number', null, ['id' => 'sss_number', 'class' => 'form-control input-sm',
                                    'placeholder' => 'SSS #' ]) }}
                                </div>

                                <div class="col-xs-4">
                                    {{ Form::label('tin_number', 'TIN #:') }}
                                    {{ Form::text('tin_number', null, ['id' => 'tin_number', 'class' => 'form-control input-sm',
                                    'placeholder' => 'TIN #' ]) }}
                                </div>
                            </div>
                        </div> <!-- Form Horizontal -->

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" id="save_employee" class="btn btn-primary">Save</button>
                    {{Form::close()}}
                </div>
            </div>
        </div>
    </div>
    {{ HTML::script('js/pages/employeeValidation.js') }}
    <script>
        $(document).on('ready', function(){
            EmployeeValidator.init();

            $('.compute_payroll').on('click',function(){
                $('#emp_id').val(($(this).attr('data-id')));    
            });

        });
    </script>
@stop


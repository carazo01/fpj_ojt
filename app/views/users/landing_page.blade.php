
<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if IE 9]>         <html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">

        <title>Accredita UK | Claims Management Portal</title>

        <meta name="description" content="Claims management portal for Accredita UK.">
        <meta name="author" content="<br />
<b>Notice</b>:  Undefined index: author in <b>C:\xampp\htdocs\template\inc\template_start.php</b> on line <b>19</b><br />
">
        <meta name="robots" content="noindex, nofollow">

        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">

        <!-- Stylesheets -->
        @include('layouts.partials.includes.styles')
        <!-- END Stylesheets -->

        <!-- Modernizr (browser feature detection library) & Respond.js (Enable responsive CSS code on browsers that don't support it, eg IE8) -->
        <script src="js/vendor/modernizr-2.7.1-respond-1.4.2.min.js"></script>
    </head>
    <body>
<!-- Login Full Background -->
<!-- For best results use an image with a resolution of 1280x1280 pixels (prefer a blurred image for smaller file size) -->
<img src="img/placeholders/backgrounds/login_bg.jpg" alt="Accredita Bg" class="full-bg animation-pulseSlow">
<!-- END Login Full Background -->

<!-- Login Container -->
<div id="login-container" class="animation-fadeIn">
    <!-- Login Title -->
    <div id="title-login" class="login-title">
        <img src="img/fpj_logo.png" class="center-block">
    </div>
    <!-- END Login Title -->

    <!-- Login Block -->
    <div class="block push-bit">
        <!-- Login Form -->
        {{--<form action="dashboard.php" method="post" id="form-login" class="form-horizontal form-bordered form-control-borderless">--}}
        {{ Form::open(array( 'action' => 'UserController@login', 'id' => 'form-login', 'class' => 'form-horizontal form-bordered form-control-borderless' ))  }}
            <div class="form-group">
                <div class="col-xs-12">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        <input type="text" id="username" name="username" class="form-control input-lg" placeholder="Username">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-12">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-asterisk"></i></span>
                        <input type="password" id="password" name="password" class="form-control input-lg" placeholder="Password">
                    </div>
                </div>
            </div>
            <div class="form-group form-actions">
                <div class="col-xs-4">
                    <label class="switch switch-primary" data-toggle="tooltip" title="Remember Me?">
                        <input type="checkbox" id="login-remember-me" name="login-remember-me" checked>
                        <span></span>
                    </label>
                </div>
                <div class="col-xs-8 text-right">
                    <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Login to Dashboard</button>
                </div>
            </div>
        {{ Form::close()  }}
        {{--</form>--}}
        <!-- END Login Form -->

    </div>
    <!-- END Login Block -->
</div>
<!-- END Login Container -->


        @include('layouts.partials.includes.scripts')
        {{ HTML::script(URL::asset('js/pages/loginValidation.js')) }}
        {{ HTML::script(URL::asset('js/pages/additional-methods.js')) }}
        <script>
        FormsValidation.init();
        </script>

    </body>
</html>
/**
 * Created by C A C on 11/28/2014.
 */

/*
 *  Document   : formsValidation.js
 *  Description: Custom javascript code used in Forms Validation page
 */

var EmployeeValidator = function() {

    return {
        init: function() {
            /*
             *  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
             */

            /* Initialize Form Validation */
            $('#form-employee').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('div').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.parent('div').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.help-block').remove();
                },
                rules: {
                    fname: {
                        required: true,
                        alpha: true,
                        minlength: 2
                    },
                    lname: {
                        required: true,
                        alpha: true,
                        minlength: 2
                    },
                    sss_number: {
                        required: true,
                        digits:true
                    },
                    tin_number: {
                        required: true,
                        digits:true
                    },
                    contact_number: {
                        required: true,
                        minlength: 5
                    },
                    address: {
                        required: true
                    },
                    position: {
                        alpha:true,
                        required: true,
                    },
                    date_hired:{
                        date:true,
                        required:true
                    },
                    rate:{
                        number:true,
                        required:true
                    }

                },
                messages: {
                    fname: {
                        required: 'Please enter a first name.',
                        alpha: 'Must be alpha characters only.',
                        minlength: 'Must consist of at least 2 characters'
                    },
                    lname: {
                        required: 'Please enter a last name.',
                        alpha: 'Must be alpha characters only.',
                        minlength: 'Must consist of at least 2 characters'
                    },
                    sss_number: {
                        digits: 'Please enter a SSS #.',
                        required: 'Please enter a SSS #.',
                        alphanumeric: 'dli ko letter',
                        minlength: 'Must consist of at least 3 characters'
                    },
                    tin_number: {
                        digits: 'Please enter a TIN #.',
                        required: 'Please provide a TIN #.',
                        minlength: 'Must be at least 5 characters long'
                    },
                    contact_number: {
                        required: 'Please provide a phone number.',
                        minlength: 'Must be at least 5 characters long'
                    },
                    address: {
                        alpha:'Please provide an address.',
                        required: 'Please provide an address.',
                        minlength: 'Must be at least 5 characters long'
                    },
                    position: {
                        alpha:'Please provide a position.',
                        required: 'Please provide a position.',
                        minlength: 'Must be at least 5 characters long'
                    },
                    date_hired: {
                        date: 'Please provide a date.',
                        required: 'Please provide a date.',
                        minlength: 'Must be at least 5 characters long'
                    },
                    rate: {
                        required: 'Please enter the rate.',
                        number: 'Must be a valid rate.'
                    }
                }
            });

            // Initialize Masked Inputs
            // a - Represents an alpha character (A-Z,a-z)
            // 9 - Represents a numeric character (0-9)
            // * - Represents an alphanumeric character (A-Z,a-z,0-9)
            $('#masked_date').mask('99/99/9999');
            $('#masked_date2').mask('99-99-9999');
            $('#masked_phone').mask('(999) 999-9999');
            $('#masked_phone_ext').mask('(999) 999-9999? x99999');
            $('#masked_taxid').mask('99-9999999');
            $('#masked_ssn').mask('999-99-9999');
            $('#masked_pkey').mask('a*-999-a999');
        }
    };
}();

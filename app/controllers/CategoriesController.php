<?php

class CategoriesController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /categories
	 *
	 * @return Response
	 */
	public function index()
	{
		$categories = Category::all();
        return View::make('inventory.categories')->withTitle('Categories')->withCategories($categories);
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /categories/create
	 *
	 * @return Response
	 */
	public function create()
	{
		$res = Category::create(Input::all());
		if( $res ){
			return json_encode(array( "msg" => "Successfully created!", "code" => "200" ));
		}
		return json_encode(array( "msg" => "Sorry, I can't process your request right now. Try again later.", "code" => "500" ));
	}


    /**public function addUser(){
        return View::make('add_user');
    }
      */

	
	public function delete(){
		if( Request::ajax() ){
			$id = Input::get('id');
			$category = Category::find($id);
			$res = $category->delete();
			
			if( $res ){
				Redirect::back()->withFlashMessage('Category successfully deleted.');
			}else{
				Redirect::back()->withFlashMessage("Sorry, we can't process your request right now!");

			}
		}
	}


	public function show(){
		$category = Category::find(Input::get('id'));

		$type = '<option value="consumable">Consumable</option>
					<option value="nonconsumable" selected>Non-Consumable</option>';

		$status = '<option value="1" selected>Active</option>
					<option value="0">Inactive</option>';

		if( $category->type == "consumable" ){
			$type = '<option value="consumable" selected>Consumable</option>
					<option value="nonconsumable">Non-Consumable</option>';
		}

		if( $category->is_active == 0 ){
			$status = '<option value="1">Active</option>
					<option value="0" selected>Inactive</option>';
		}

		$html = '
			<iframe src="about:blank" id="remember" class="hidden" name="remember"></iframe>
				<form id="frm_update_category" class="form" method="post" action="about:blank" target="remember" data-id="'.$category->id.'">
					<div class="form-group">
						<label for="type">Consumable</label>
						<select id="type" class="form-control">'.$type.'</select>
					</div>
					<div class="form-group">
						<label for="description">Description</label>
						<input type="text" id="description" class="form-control" value="'.$category->description.'">
					</div>
					<div class="form-group">
						<label for="status">Status</label>
						<select class="form-control" id="status">'.$status.'</select>	
					</div>
				</form>
		';
		return $html;
	}


	/**
	 * Show the form for editing the specified resource.
	 * GET /categories/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit()
	{
		if( Request::ajax() ){
			$id = Input::get('id');
			$description = Input::get("description");
			$type = Input::get("type");
			$is_active = Input::get("status");

			$category = Category::find($id);
			$category->description = $description;
			$category->type = $type;
			$category->is_active = $is_active;
			if($category->save()){
				return json_encode(array("msg"=>"Successfully updated!", "code"=>"200"));
			}
			return json_encode(array(
				"msg" => "Sorry, we can't process your request right now.\n Please contact system developers.!", 
				"code" => "500")
			);

		}
	}



}
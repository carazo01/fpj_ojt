<?php

class DtrController extends \BaseController {

	public function employees()
	{
		$employee = Employee::with('dtrToday')->get();
		return View::make('dtr.employees')->withEmployee($employee);
	}

	public function store()
	{
		Employee::create(Input::all());
		return Redirect::back();
	}

	public function login()
	{

		$dtr = New Dtr;

		$dtr->employee_id = Input::get('id');
		$dtr->login_time = DB::raw('now()');
		$dtr->date_login = DB::raw('now()');
		$dtr->save();

		return Redirect::back()->withFlashMessage('Logged User!');

	}

	public function logout()
	{
		$dtr = Dtr::where('employee_id',Input::get('id'))
					->where('date_login',date('Y-m-d'))->first();

		$dtr->logout_time = DB::raw('now()');

		$dtr->save();

		return Redirect::back()->withFlashMessage('Logged User!');
	}

	public function delete()
	{
		$employee = Employee::find(Input::get('id'));

		$employee->delete();

		return $employee;
	}


}
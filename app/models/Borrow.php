<?php

class Borrow extends \Eloquent {
	protected $guarded = ['id'];

	protected $table = "borrow";

	public function master(){
		return $this->belongsTo('BorrowMaster');
	}
}
/*
 *  Document   : formsValidation.js
 *  Description: Custom javascript code used in Forms Validation page
 */

var AddUserValidation = function() {

    return {
        init: function() {
            console.log('test11');

            /*
             *  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
             */
            /* Initialize Form Validation */
            $('#frm-addUser').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    e.closest('.form-group').removeClass('has-success has-error');
                    e.closest('.help-block').remove();
                },
                rules: {
                    lname: {
                        required: true,
                        minlength: 3

                    },

                    fname: {
                        required: true,
                        minlength: 3
                        
                    },

                    mname: {
                        required: true,
                        minlength: 3
                        
                    },

                    address: {
                        required: true,
                        minlength: 3
                        
                    },

                    birthday: {
                        required: true,
                    },

                    username: {
                        required: true,
                        minlength: 3
                        
                    },

                    
                    password: {
                        required: true,
                        minlength: 5
                    },

                    confirm_password: {
                        required: true,
                        equalTo: '#inputPassword'
                    }
                   
                },

                messages: {
                    lname: {
                        required: 'Please enter your lastname!',
                        minlength: 'Your username must consist of at least 3 characters'
                    },

                    fname: {
                        required: 'Please enter a firstname',
                        minlength: 'Your username must consist of at least 3 characters'
                    },

                    mname: {
                        required: 'Please enter a middlename',
                        minlength: 'Your username must consist of at least 3 characters'
                    },

                    address: {
                        required: 'Please enter a address',
                        minlength: 'Your username must consist of at least 3 characters'
                    },

                    birthday: {
                        required: 'Please enter a birthday',
                        minlength: 'Your username must consist of at least 3 characters'
                    },

                    username: {
                        required: 'Please enter a username',
                        minlength: 'Your username must consist of at least 3 characters'
                    },

                   
                    password: {
                        required: 'Please provide a password',
                        minlength: 'Your password must be at least 5 characters long'
                    },

                    confirm_password: {
                        required: 'Please provide a password',
                        minlength: 'Your password must be at least 5 characters long',
                        equalTo: 'Please enter the same password as above'
                    }
                    
                }
            });

            // Initialize Masked Inputs
            // a - Represents an alpha character (A-Z,a-z)
            // 9 - Represents a numeric character (0-9)
            // * - Represents an alphanumeric character (A-Z,a-z,0-9)
            $('#masked_date').mask('99/99/9999');
            $('#masked_date2').mask('99-99-9999');
            $('#masked_phone').mask('(999) 999-9999');
            $('#masked_phone_ext').mask('(999) 999-9999? x99999');
            $('#masked_taxid').mask('99-9999999');
            $('#masked_ssn').mask('999-99-9999');
            $('#masked_pkey').mask('a*-999-a999');
        }
    };
}();

@extends('layouts.default')

@section('content')

    <div class="content-header">
        <div class="header-section">
            <h1>Employees</h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li>Payroll</li>



        <li><a href="">Employee</a></li>


    </ul>
    <div class="block">
        <!-- Title -->
        <div class="block-title">
            <h2><strong>Employee</strong></h2>
            <a href="javascript:void(0);" data-toggle="modal" data-target="#addEmployee">Add new</a>
        </div>

        <div class="table-responsive">
            <table class="datatable table table-striped table-bordered table-hover table-condensed" id="tbl_categories">

                <thead>
                <td>Name</td>
                <td>Contact Number</td>
                <td>Position</td>
                <td>Time Tracker</td>
                <td>Action</td>
                </thead>

                @foreach($employee as $e)
                    <tr data-id="{{ $e->id }}">
                        <td>{{ $e->fname .  ' ' . $e->lname }}</td>
                        <td>{{ $e->contact_number }}</td>
                        <td>{{$e->position }}</td>
                        <td>

                            {{Form::open(['action' => 'dtr.login'])}}
                                <input type="hidden" name="id" value="{{$e->id}}"/>
                            <button type="submit" class="btn-mini {{isNotLogged($e->dtr_today->first())}} btn-success btn inline" ><i class="fa fa-clock-o"></i> Login</button>
                            {{Form::close()}}
                            {{Form::open(['action' => 'dtr.logout'])}}
                                <input type="hidden" name="id" value="{{$e->id}}"/>
                            <button type="submit" class="btn-mini  {{isLogged($e->dtr_today->first())}} btn btn-danger"><i class="fa fa-times-circle-o"></i> Logout</button>
                            {{Form::close()}}

                            @if(isLogged($e->dtr_today->first()))
                                  {{ '<span class="label-success label" >Time has been logged. </span> '}}
                              @endif

                        </td>
                        <td>
                            <a href="javascript:void(0);" data-toggle="modal" data-target="#editCategory" data-id="{{ $e->id }}" class="edit_employee btn btn-primary btn-mini"><i class="fa fa-pencil-square-o"></i> Edit</a>
                            <a href="javascript:void(0);" data-id="{{ $e->id }}" data-onclick="delete-employee" class="btn btn-mini btn-danger"><i class="fa fa-remove"></i> Delete</a>
                        </td>
                    </tr>
                @endforeach

            </table>
        </div>
    </div>





@stop
@section('footer')
    <!-- Button trigger modal -->
    <!-- <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#editCategory">
      Launch demo modal
    </button> -->

    <!-- Modal -->
    <div class="modal fade" id="editCategory" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit Categories</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="update_categories">Save changes</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal for Add new Category -->
    <div class="modal fade" id="addEmployee" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add Employee</h4>
                </div>
                <div class="modal-body">
                    {{Form::open(['route' => 'employee.save','id' => 'form-employee'])}}
                        <!-- Last Name Form Input -->
                        <div class="form-horizontal">
                            <div class="form-group">
                                <div class="col-xs-6">
                                    {{ Form::label('fname', 'First Name:') }}
                                    {{ Form::text('fname', null, ['id' => 'fname', 'class' => 'form-control input-sm',
                                    'placeholder' => 'First Name' ]) }}
                                </div>
                                <div class="col-xs-6">
                                    {{ Form::label('lname', 'Last Name:') }}
                                    {{ Form::text('lname', null, ['id' => 'lname', 'class' => 'form-control input-sm',
                                    'placeholder' => 'Last Name' ]) }}
                                </div>
                            </div>
                            <!-- Address Form Input -->
                            <div class="form-group">
                                <div class="col-md-12">
                                    {{ Form::label('address', 'Address:') }}
                                    {{ Form::text('address', null, ['id' => 'address', 'class' => 'form-control' ]) }}
                                </div>
                            </div>
                            <!-- Address Form Input -->
                            <div class="form-group">
                                <div class="col-md-6">
                                    {{ Form::label('contact_number', 'Phone:') }}
                                    {{ Form::text('contact_number', null, ['id' => 'contact_number', 'class' => 'form-control' ]) }}
                                </div>
                            </div>
                            <hr/>
                            <div class="form-group">
                                <div class="col-xs-4">
                                    {{ Form::label('position', 'Position:') }}
                                    {{ Form::text('position', null, ['id' => 'position', 'class' => 'form-control input-sm',
                                    'placeholder' => 'Position' ]) }}
                                </div>

                                    <div class="col-xs-4">
                                        {{ Form::label('rate', 'Daily Rate:') }}
                                        {{ Form::text('rate', null, ['id' => 'rate', 'class' => 'form-control input-sm',
                                        'placeholder' => 'Daily Rate' ]) }}
                                    </div>

                                <div class="col-xs-4">
                                    {{ Form::label('date_hired', 'Date Hired:') }}
                                    {{ Form::text('date_hired', null, ['id' => 'date_hired', 'class' => 'input-datepicker form-control input-sm']) }}
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-4">
                                    {{ Form::label('sss_number', 'SSS #:') }}
                                    {{ Form::text('sss_number', null, ['id' => 'sss_number', 'class' => 'form-control input-sm',
                                    'placeholder' => 'SSS #' ]) }}
                                </div>

                                <div class="col-xs-4">
                                    {{ Form::label('tin_number', 'TIN #:') }}
                                    {{ Form::text('tin_number', null, ['id' => 'tin_number', 'class' => 'form-control input-sm',
                                    'placeholder' => 'TIN #' ]) }}
                                </div>
                            </div>
                        </div> <!-- Form Horizontal -->

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" id="save_employee" class="btn btn-primary">Save</button>
                    {{Form::close()}}
                </div>
            </div>
        </div>
    </div>
    {{ HTML::script('js/pages/employeeValidation.js') }}
    <script>
        $(document).on('ready', function(){
            EmployeeValidator.init();
        });
    </script>
@stop


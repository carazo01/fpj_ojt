<?php 

class UserController extends \BaseController {

	public function index()
	{
		$users = User::all();

		return View::make('users.index', ['users' => $users]);

	}

	public function show($username)
	{

		$user = User::whereUsername($username)->first();

		return View::make('users.show', ['user'=>$user]);
	}

    public function show_usersPage() {
        $users = User::all();

        return View::make('users.users_page')->withTitle('Users')->withUsers($users);
    }

    public function showUserInfo() {
        $user = User::find(Input::get('id'));

        return $user->toJSON();

    }




    public function login()
    {
        if( Auth::attempt( array('username'=>Input::get('username'), 'password'=>Input::get('password') ) ) )
        {
            return Redirect::route('dashboard');
        }
        return Redirect::back()->withInput();
    }

    public function dashboard()
    {
        return View::make('users.show');
    }

    public function create()
    {

//        $users = User::create(Input::all());
        $user = new User;
        $user->lastname = ucfirst(Input::get('lname'));
        $user->firstname = ucfirst(Input::get('fname'));
        $user->middlename = ucfirst(Input::get('mname'));
        $user->address = Input::get('address');
        $user->birthday = Input::get('birthday');
        $user->username = Input::get('username');
        $user->password = Hash::make(Input::get('password'));
        
        $user->save();

        return \Redirect::back();

        if( $user ){
            return json_encode(array( "msg" => "Successfully created!", "code" => "200" ));
        }
        return json_encode(array( "msg" => "Sorry, I can't process your request right now. Try again later.", "code" => "500" ));
    }

    public function edit()
    {
            $id = Input::get('editId');
            //$username = Input::get("username");
            //$password = Input::get("password");
            $lastname = Input::get("lastname");
            $firstname = Input::get("firstname");
            //$middlename = Input::get("middlename");
            $address = Input::get("address");
            $birthday = Input::get("birthday");



            $user = User::findOrFail($id);
            //$user->username = $username;
            $user->lastname = ucfirst($lastname);
            $user->firstname = ucfirst($firstname);
            //$user->middlename = $middlename;
            $user->address = $address;
            $user->birthday = $birthday;
            $user->save();

           return Redirect::back();
    }

    public function delete(){
        if( Request::ajax() ){
            $id = Input::get('id');
            $user = User::find($id);
            $res = $user->delete();

            if( $res ){
                Redirect::back()->withFlashMessage('Category successfully deleted.');
            }else{
                Redirect::back()->withFlashMessage("Sorry, we can't process your request right now!");

            }
        }
    }
}

<?php

class BorrowMaster extends \Eloquent {
	protected $guarded = ['id'];

	protected $table = "borrow_master";

	public function borrows(){
		return $this->hasMany('Borrow');
	}
}
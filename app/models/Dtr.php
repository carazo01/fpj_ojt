<?php

class Dtr extends \Eloquent {
	protected $guarded = ['id'];

	protected $table = "dtr";

	public function employee()
	{
		return $this->belongsTo('Employee');
	}

}
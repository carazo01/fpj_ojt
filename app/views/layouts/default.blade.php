
<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if IE 9]>         <html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">

        <title>{{ isset($title)? $title : 'Accredita UK | Claims Management Portal' }}</title>

        <meta name="description" content="Claims management portal for Accredita UK.">
        <meta name="author" content="">
        <meta name="robots" content="noindex, nofollow">

        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">

        <!-- Stylesheets -->
        @include('layouts.partials.includes.styles')
        <!-- END Stylesheets -->

        <!-- Modernizr (browser feature detection library) & Respond.js (Enable responsive CSS code on browsers that don't support it, eg IE8) -->
        <script src="{{ URL::asset('js/vendor/modernizr-2.7.1-respond-1.4.2.min.js') }}"></script>
    </head>
    <body>
<!-- Page Wrapper -->

<div id="page-wrapper" class="page-loading">
    <!-- Preloader -->
    <!-- Preloader functionality (initialized in js/app.js) - pageLoading() -->
    <!-- Used only if page preloader is enabled from inc/config (PHP version) or the class 'page-loading' is added in #page-wrapper element (HTML version) -->
    <div class="preloader themed-background">
        <div class="inner">
            <h3 class="text-light visible-lt-ie9 visible-lt-ie10"><strong>Loading..</strong></h3>
            <div class="preloader-spinner hidden-lt-ie9 hidden-lt-ie10"></div>
        </div>
    </div>
    <!-- END Preloader -->

    <!-- Page Container -->
    <!-- In the PHP version you can set the following options from inc/config file -->

    <div id="page-container" class="sidebar-partial sidebar-visible-lg sidebar-no-animations">
        <!-- Sidebar -->

        <!-- END Main Sidebar -->
            @include('layouts.partials.sidebar')

        <!-- Main Container -->
        <div id="main-container">
            <!-- Header -->

            @include('layouts.partials.header')
            <!-- Page content -->
            <div id="page-content">
                <!-- Dashboard Header -->
                <!-- For an image header add the class 'content-header-media' and an image as in the following example -->
                @include('layouts.partials.main_title')
                <!-- END Dashboard Header -->

                @yield('content')

            </div>
            <!-- END Page Content -->

            <!-- Footer -->

            @include('layouts.partials.footer')



            <!-- END Footer -->
        </div>
        <!-- END Main Container -->
    </div>
    <!-- END Page Container -->
</div>
<!-- END Page Wrapper -->


<!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
<a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

<!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->

<!-- END User Settings -->





@include('users.manageUserModal')

@include('users._editmodaluser')
@include('layouts.partials.includes.scripts')
@yield('footer')

{{ HTML::script(URL::asset('js/pages/addUser_validation.js')) }}
{{ HTML::script(URL::asset('js/pages/additional-methods.js')) }}
<script>
    AddUserValidation.init();
    console.log('test');
</script>

    </body>
</html>
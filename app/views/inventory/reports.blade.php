@extends('layouts.default')

@section('content')
	<table class="table datatable table-bordered background_white">
	    <thead>
	        <td>Image</td>
	        <td>Name</td>
	        <td>Description</td>
	        <td>Serial Number</td>
	        <td>Status</td>
	        <td>Category</td>
	    </thead>
      	@foreach($items as $item)
        <tr>
          <td>{{ $item->image }}</td>
          <td>{{ $item->name }}</td>
          <td>{{ $item->description }}</td>
          <td>{{ $item->serial_number }}</td>
          <td>{{ ($item->is_active == 1) ? 'Yes' : 'No' }}</td>
          <td>{{ $item->category->description }}</td>
        </tr>
      	@endforeach
    </table>
@stop
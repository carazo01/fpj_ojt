<!-- Icons -->
<link rel="shortcut icon" href="img/favicon.ico">
<link rel="apple-touch-icon" href="img/icon57.png" sizes="57x57">
<link rel="apple-touch-icon" href="img/icon72.png" sizes="72x72">
<link rel="apple-touch-icon" href="img/icon76.png" sizes="76x76">
<link rel="apple-touch-icon" href="img/icon114.png" sizes="114x114">
<link rel="apple-touch-icon" href="img/icon120.png" sizes="120x120">
<link rel="apple-touch-icon" href="img/icon144.png" sizes="144x144">
<link rel="apple-touch-icon" href="img/icon152.png" sizes="152x152">
<!-- END Icons -->

{{ HTML::style('/css/bootstrap.min.css')  }}
{{ HTML::style('/css/plugins.css')  }}
{{ HTML::style('/css/main.css') }}
{{ HTML::style('/css/themes/accredita.css') }}
{{ HTML::style('/css/themes.css') }}
{{ HTML::style('/css/styles.css') }}
{{ HTML::style('/css/jquery.dataTables.min.css') }}


<?php

class Category extends \Eloquent {
	protected $guarded = ['id'];

	protected $table = "categories";

	public function items(){
		return $this->hasMany('Item', 'id');
	}
}
<?php
use FPJ\Repositories\AjaxRepository;
use FPJ\Repositories\ToolRepository;
class AjaxController extends \BaseController {

	public function __construct(AjaxRepository $ajax, ToolRepository $tool){
		$this->ajax = $ajax;
		$this->tool = $tool;
	}

	/**
	 * Display a listing of the resource.
	 * GET /ajax
	 *
	 * @return Response
	 */
	public function index()
	{
		if( Request::ajax() ){
			$q = Input::get('q');
			if( $q == 'return_tool' ){
				if( $this->ajax->return_tool( Input::all() ) ){
					return json_encode( array( "status_code" => "200", "msg" => "Tool successfully returned." ) );
				}else{
					return json_encode( array( "status_code" => "500", "msg" => "Sorry, something went wrong. Please try again later." ) );
				}
			}else if( $q == "update_tool" ){
				$res = $this->tool->update_tool( Input::all() );
				if( $res ){
					return json_encode( array( "status_code" => "200", "msg" => "Tool successfully updated." ) );
				}else{
					return $res;
				}
			}
		}
	}

}
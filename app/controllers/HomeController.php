<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showWelcome()
	{
		return View::make('hello');
	}

    public function showDashboard()
    {
        return View::make('admin.dashboard');
    }

    public function showLandingPage()
    {
        return View::make('users.landing_page');
    }
    public function showInventoryCategories()
    {
        return View::make('inventory.categories');
    }
    public function showInventoryItems()
    {
    	$items = Item::all();
        return View::make('inventory.items')->withItems($items);
    }

}

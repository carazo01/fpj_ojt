<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInventoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('inventory', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('item_id')->index()->unsigned();;
            $table->foreign('item_id')
                ->references('id')->on('items')
                ->onDelete('cascade');
			$table->string('employee_id');
			$table->double('quantity');
			$table->string('type');
			$table->dateTime('date_barrowed');
			$table->dateTime('date_returned');
			$table->dateTime('date_consumed');
			$table->boolean('is_active');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('inventory');
	}

}

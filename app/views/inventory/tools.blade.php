@extends('layouts.default')

@section('content')
<ul class="breadcrumb breadcrumb-top">
        <li>Awwwww</li>
        <li><a href="">Master</a></li>
</ul>
<div class="block">
        <!-- Title -->
        <div class="block-title">
            <h2><strong>gegege</strong></h2>
            <a href="#" data-toggle="modal" data-target="#add_tool_modal">Add new</a>
        </div>
   	<div class="table-responsive" id="tools_table">
    	<table class="table table-bordered background_white" id="inner_tools_table">
        <thead>
          <td>Name</td>
          <td>Description</td>
          <td>Type</td>
  	      <td>Serial Number</td>
  	      <td>Quantity</td>
          <td>Unit</td>
  	      <td>Action</td>
  	    </thead>
        <tbody></tbody>
      </table>
   	</div>
</div>


<!-- Modal for Add new tool -->
  <div class="modal fade" id="add_tool_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Add new tool</h4>
        </div>
          <iframe class="hidden" name="remember" id="remember" src="about:blank"></iframe>
          <form class="form" method="post" action="" target="remember" id="form_addtool">
          <div class="modal-body">
          	<div class="form-group">
              <label for="tool_description">Name</label>
              <input type="text" name="name" class="form-control" id="tool_description" placeholder="Tool Name">
          	</div>
          	<div class="form-group">
              <label for="tool_description">Description</label>
              <textarea class="form-control" name="description" placeholder="Tool Description"></textarea>
          	</div>
          	<div class="form-group">
              <label for="tool_type">Type</label>
              <select id="tool_type" name="is_consumable" class="form-control">
                <option value="1" selected>Consumable</option>
                <option value="0">Non-Consumable</option>
              </select>
          	</div>
          	<div class="form-group">
          		<label for="tool_quantity">Quantity</label>
          		<input class="form-control" id="tool_quantity" name="quantity" placeholder="Quantity" data-type="number" value="1">
          	</div>
            <div id="serials" data-for="nonconsumable" class="form-group"></div>
          	<div class="form-group" data-for="consumable">
          		<label for="tool_unit">Unit</label>
          		<select name="unit" id="tool_unit" class="form-control">
          			<option title="pieces">pcs.</option>
          			<option title="boxes">box</option>
          			<option title="meters">m</option>
          			<option title="kilograms">kg</option>
          		</select>
          	</div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" id="save_tool" data-action="add_new" class="btn btn-primary">Save</button>
          </div>
        </form>
      </div>
    </div>
  </div>

<!-- Modal for edit tools -->
  <div class="modal fade" id="edit_tool_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Update tool</h4>
        </div>
          <iframe class="hidden" name="remember" id="remember" src="about:blank"></iframe>
          <form class="form" method="post" action="" target="remember" id="form_edittool">
          <div class="modal-body">
            <div class="form-group">
              <label for="tool_description">Name</label>
              <input type="text" name="name" class="form-control" id="tool_description" placeholder="Tool Name">
            </div>
            <div class="form-group">
              <label for="tool_description">Description</label>
              <textarea class="form-control" name="description" placeholder="Tool Description"></textarea>
            </div>
            <div class="form-group">
              <label for="tool_type">Type</label>
              <select id="tool_type" name="is_consumable" class="form-control">
                <option value="1" selected>Consumable</option>
                <option value="0">Non-Consumable</option>
              </select>
            </div>
            <div class="form-group">
              <label for="tool_quantity">Quantity</label>
              <input class="form-control" id="tool_quantity" name="quantity" placeholder="Quantity" data-type="number" value="1">
            </div>
            <div id="serials" data-for="nonconsumable" class="form-group"></div>
            <div class="form-group" data-for="consumable">
              <label for="tool_unit">Unit</label>
              <select name="unit" id="tool_unit" class="form-control">
                <option title="pieces">pcs.</option>
                <option title="boxes">box</option>
                <option title="meters">m</option>
                <option title="kilograms">kg</option>
              </select>
              <input type="hidden" name="q" value="update_tool"/>
              <input type="hidden" name="id" value=""/>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" id="update_tool" data-action="add_new" class="btn btn-primary">Save</button>
          </div>
        </form>
      </div>
    </div>
  </div>
@stop
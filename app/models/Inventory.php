<?php

class Inventory extends \Eloquent {
	protected $guarded = ['id'];

	protected $table = "inventory";

	public function items(){
		return $this->belongsTo('Item');
	}
}
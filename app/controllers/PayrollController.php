<?php

class PayrollController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /payroll
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$employee = Employee::all();
		return View::make('payroll.payroll')->withEmployee($employee);
	}

	public function employeeName()
	{
		$employee = Employee::all();
	}

	public function compute()
	{

		$dateFrom = Input::get('fromDate');
		$dateTo = Input::get('toDate');
		$employee = Dtr::selectRaw('SUM(TIME_TO_SEC(dtr.login_time - maketime(8,0,0))/60) as late,
                        COUNT(dtr.id) as daysOfWork, SUM(TIME_TO_SEC(dtr.logout_time - maketime(17,0,0))/60) as overtime,
                        employees.*
                    ')
					->join('employees', 'employees.id','=','dtr.employee_id')
					->where('employee_id',Input::get('emp_id'))
					->whereBetween('date_login', [date('Y-m-d',strtotime($dateFrom)), date('Y-m-d',strtotime($dateTo))])
					->first();

		return $employee;
	}

}
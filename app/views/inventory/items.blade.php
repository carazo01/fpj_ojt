
@extends('layouts.default')

@section('content')

<ul class="breadcrumb breadcrumb-top">




        <li><a href=""></a></li>


</ul>


<div class="block">
        <!-- Title -->
        <div class="block-title">
            <h2><strong></strong></h2>
            <a href="javascript:void(0);" data-toggle="modal" data-target="#add_item_modal">Add new</a>
        </div>
   <div class="table-responsive">
	<table class="datatable table table-bordered background_white">

	    <thead>
	        <!-- <td>Image</td> -->
	        <td>Name</td>
	        <td>Description</td>
	        <td>Serial Number</td>
	        <td>Status</td>
	        <td>Category</td>
	        <td>Action</td>
	    </thead>

        @foreach($items as $item)
            <tr data-id="{{ $item->id }}">
            <!-- <td>{{ $item->img}}</td> -->
            <td>{{ $item->name }}</td>
            <td>{{ $item->description }}</td>
            <td>{{ $item->serial_number}}</td>
            <td>{{ ($item->status == 1)? 'Active' : 'Inactive' }}</td>
            <td>
              {{ $item->category->description }} <!-- di mu gana ang relationship sa items og categories -->
            </td>
            <td>
              <a href="javascript:void(0);" data-toggle="modal" data-target="#editItem" data-id="{{ $item->id }}" class="edit_item">Edit</a>
              <a href="javascript:void(0);" data-id="{{ $item->id }}" data-onclick="delete-item">Delete</a>
            </td>
            </tr>
        @endforeach
    </table>
   </div>
  </div>

<!-- Modal for edit items -->

  <div class="modal fade" id="editItem" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Edit Items</h4>
        </div>
        <div class="modal-body">
          <form class="form">
            <!-- <div class="form-group">
              <label class="sr-only" for="editItem_image">Image</label>
              <input type="text" class="form-control" id="editItem_image" placeholder="Image">
            </div> -->
            <div class="form-group">
              <label class="sr-only" for="editItem_name">Name</label>
              <input type="text" class="form-control" id="editItem_name" placeholder="Name">
            </div>
            <div class="form-group">
              <label class="sr-only" for="editItem_description">Description</label>
              <input type="text" class="form-control" id="editItem_description" placeholder="Description">
            </div>
            <div class="form-group">
              <label class="sr-only" for="ediIitem_serialNumber">Serial Number</label>
              <input type="text" class="form-control" id="ediIitem_serialNumber" placeholder="Serial Number">
            </div>
            <div class="form-group">
              <label class="sr-only" for="editItem_status">Status</label>
              <select id="editItem_status" class="form-control">
                <option value="1">Active</option>
                <option value="0">Inactive</option>
              </select>
            </div>
            <div class="form-group">
              <label class="sr-only" for="editItem_category">Category</label>
               {{ Form::select('editItem_category', $categories, "", ["id" => "editItem_category", "class" => "form-control"]) }}
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" id="btn_updateItems">Save changes</button>
        </div>
      </div>
    </div>
  </div>

<!-- Modal for Add new Item -->
<div class="modal fade" id="add_item_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add Items</h4>
      </div>
      <div class="modal-body">
        <form class="form">
          <div class="form-group">
            <!-- <label class="sr-only" for="item_image">Image</label> -->
            <!-- <input type="text" class="form-control" id="item_image" placeholder="Image"> -->
            <label class="sr-only" for="item_name">Name</label>
            <input type="text" class="form-control" id="item_name" placeholder="Name">
            <label class="sr-only" for="item_description">Description</label>
            <input type="text" class="form-control" id="item_description" placeholder="Description">
            <label class="sr-only" for="item_serialNumber">Serial Number</label>
            <input type="text" class="form-control" id="item_serialNumber" placeholder="Serial Number">
            <label class="sr-only" for="item_status">Status</label>
            <input type="hidden" class="form-control" id="item_status" value="1" placeholder="Status">
            <label class="sr-only" for="item_category">Category</label>
            {{ Form::select('item_category', $categories, "", ["id" => "item_category", "class" => "form-control"]) }}
          </div>
        </form>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" id="save_item" class="btn btn-primary">Save</button>
      </div>
    </div>
  </div>
</div>

@stop



@extends('layouts.default')

@section('content')

<div class="content-header">
    <div class="header-section">
        <h1>Categories</h1>
    </div>
</div>
<ul class="breadcrumb breadcrumb-top">
        <li>Categories</li>



        <li><a href=""></a></li>


</ul>
<div class="block">
        <!-- Title -->
        <div class="block-title">
            <h2><strong></strong></h2>
            <a href="javascript:void(0);" data-toggle="modal" data-target="#add_category_modal">Add new</a>
        </div>

    <div class="table-responsive">
        <table class="datatable table table-striped table-bordered table-hover table-condensed" id="tbl_categories">

            <thead>
              <td>Type</td>
              <td>Description</td>
              <td>Is Active</td>
              <td>Action</td>
            </thead>

            @foreach($categories as $category)
              <tr data-id="{{ $category->id }}">
                <td>{{ $category->type }}</td>
                <td>{{ $category->description }}</td>
                <td>{{ ($category->is_active == 0) ? 'No' : 'Yes' }}</td>
                <td>
                  <a href="javascript:void(0);" data-toggle="modal" data-target="#editCategory" data-id="{{ $category->id }}" class="edit_category">Edit</a>
                  <a href="javascript:void(0);" data-id="{{ $category->id }}" data-onclick="delete-category">Delete</a>
                </td>
              </tr>
            @endforeach

        </table>
    </div>
</div>


<!-- Button trigger modal -->
<!-- <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#editCategory">
  Launch demo modal
</button> -->

<!-- Modal -->
<div class="modal fade" id="editCategory" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit Categories</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="update_categories">Save changes</button>
      </div>
    </div>
  </div>
</div>


<!-- Modal for Add new Category -->
<div class="modal fade" id="add_category_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add Category</h4>
      </div>
      <div class="modal-body">
        <form class="form">
          <div class="form-group">
            <label class="sr-only" for="category_type">Type</label>
            <select id="category_type" class="form-control">
              <option value="consumable">Consumable</option>
              <option value="nonconsumable">Non-Consumable</option>
            </select>
          </div>
          <div class="form-group">
            <label class="sr-only" for="category_description">Name</label>
            <input type="text" class="form-control" id="category_description" placeholder="Category Name">
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" id="save_category" class="btn btn-primary">Save</button>
      </div>
    </div>
  </div>
</div>
@stop


@extends('layouts.default')

@section('content')
<ul class="breadcrumb breadcrumb-top">
        <li>Awwwww</li>
        <li><a href="">Master</a></li>
</ul>
<div class="block">
    <!-- Title -->
    <div class="block-title">
        <h2><strong>Tools Inventory Management</strong></h2>
    </div>
    <div class="table-responsive" id="tools_table">
      <a class="btn-mini btn table-refresher" data-table="borrow_tools_table">Refresh</a>
      <table class="table table-bordered background_white" id="borrow_tools_table">
        <thead>
          <td>Name</td>
          <td>Description</td>
          <td>Type</td>
          <td>Serial Number</td>
          <td>Quantity</td>
          <td>Unit</td>
          <td>Action</td>
        </thead>
        <tbody></tbody>
      </table>
    </div>
</div>

<!-- Modal for Borrowing tools -->
<div class="modal fade" id="borrowtool" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Select Employees Responsible</h4>
      </div>
        <iframe class="hidden" name="remember" id="remember" src="about:blank"></iframe>
        <form class="form" method="post" action="" target="remember" id="form_borrowtool">
        <div class="modal-body">
          <label>Tool Information</label>
          <div  id="tool_information">
            <div class="form-group">
              <label>Name</label>
              <input class="form-control" type="text" readonly id="tool_name" placeholder="Tool Name"> 
            </div>
            <div class="form-group">
              <label>Description</label>
              <textarea class="form-control" readonly id="tool_description" placeholder="Tool Description"></textarea> 
              
            </div>
            <div class="form-group">
              <label>Serial No.</label>
              <input class="form-control" type="text" readonly placeholder="Serial No." id="tool_serial">
              <input type="hidden" name="tool_id">
              <input type="hidden" name="option" value="borrow">
            </div>
          </div>
          <div class="form-group">
            <label for="tool_description">Employee</label>
            <select id="responsible_employees" name="responsible_employees[]" class="form-control" multiple="multiple"></select>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" id="btn_borrow_tool" class="btn btn-primary">Borrow</button>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="updatequantity" tabindex="-1" role="dialog" aria-labelledby="updatequantityLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="updatequantityLabel">Update Quantity for this Consumable</h4>
      </div>
        <iframe class="hidden" name="remember" id="remember" src="about:blank"></iframe>
        <form class="form" method="post" action="" target="remember" id="form_updatequantity">
        <div class="modal-body">
          <div class="form-group" id="div_updatequantity">
            <label>New Quantity</label>
            <div class="input-group">
              <input class="form-control" data-type="number" type="text" name="new_quantity" id="new_quantity">
              <span class="input-group-btn">
                <span class="btn btn-default" id="unit">pcs</span>
              </span>
            </div>
            <input type="hidden" name="option" value="updatequantity">
            <input type="hidden" name="id">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Update</button>
        </div>
      </form>
    </div>
  </div>
</div>


<div class="modal fade" id="getborrowers" tabindex="-1" role="dialog" aria-labelledby="getborrowersLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="getborrowersLabel">Borrowed By</h4>
      </div>
      <div class="modal-body"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button class="btn btn-success" id="return_tool">Return</button>
      </div>
    </div>
  </div>
</div>
@stop
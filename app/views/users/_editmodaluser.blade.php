<!-- Modal -->
<div class="modal fade" id="edit_users_modal" tabindex="-1" role="dialog" aria-labelledby="edit_users_modalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="edit_users_modalLabel">Edit User</h4>
      </div>
      {{Form::open(['action' => 'UserController@edit'])}}

      <div class="modal-body">
        <div class="form-group">
          <input name="editId" type="hidden" id="editId" />
          <label for="inputName">First Name</label>
            <input name="firstname" class="form-control" id="editFirstName" placeholder="Enter Name">
          </div>
          <div class="form-group">
            <label for="inputLastname">Lastname</label>
            <input name="lastname" class="form-control" id="editLastname" placeholder="Lastname">
          </div>
            <div class="form-group">
                <label for="inputAddress">Address</label>
                <input name="address" class="form-control" id="editAddress" placeholder="Enter Address">
            </div>
            <div class="form-group">
                <label for="inputBirthday">Birthday</label>
                <input name="birthday"  class="form-control datepicker" id="editBirthday" placeholder="Enter Birthday">
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>

    </div>
  </div>
</div>

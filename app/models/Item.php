<?php

class Item extends \Eloquent {
	protected $guarded = ['id'];

	protected $table = "items";

	public function category(){
		return $this->belongsTo('Category', 'category_id');
	}

	public function inventory(){
		return $this->hasMany('Inventory');
	}
}
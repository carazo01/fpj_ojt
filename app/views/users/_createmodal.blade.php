<!-- Modal -->
<div class="modal fade" id="manage_users_modal" tabindex="-1" role="dialog" aria-labelledby="manage_users_modalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="manage_users_modalLabel">Add User</h4>
      </div>
      <div class="modal-body">
        {{Form::open(['action' => 'UserController@create'])}}
        <div class="form-group">
            <label for="inputName">Name</label>
            <input type="name" class="form-control" id="inputName" placeholder="Enter Name">
          </div>
          <div class="form-group">
            <label for="inputLastname">Lastname</label>
            <input type="lastname" class="form-control" id="inputLastname" placeholder="Lastname">
          </div>
            <div class="form-group">
                <label for="inputEmail">Email</label>
                <input type="email" class="form-control" id="inputEmail" placeholder="Enter Email">
            </div>
            <div class="form-group">
                <label for="inputAddress">Address</label>
                <input type="email" class="form-control" id="inputAddress" placeholder="Enter Address">
            </div>
            <div class="form-group">
                <label for="inputBirthday">Birthday</label>
                <input type="text" class="form-control datepicker" id="inputBirthday" placeholder="Enter Birthday">
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
    </div>
      {{Form::close()}}
  </div>
</div>
